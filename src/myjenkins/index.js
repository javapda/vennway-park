const _ = require('lodash')
const jenkinsConfig = {
    credentials : null,
    host : 'gs-ci.ad.goldblattsystems.com',
    ssl : false,
    getUrl : () => `http${jenkinsConfig.ssl?'s':''}://${jenkinsConfig.credentials?jenkinsConfig.credentials+'@':''}${jenkinsConfig.host}`
}
const jenkinsCredentials = 'jenkins_jedi:hampster'
const jenkinsHost = 'gs-ci.ad.goldblattsystems.com'
const jenkinsUrl = `http://${jenkinsCredentials}@${jenkinsHost}`
const jenkins = require('jenkins')({ baseUrl: `${jenkinsUrl}`, crumbIssuer: true, promisify: true });


class MyJenkins {
    constructor(_config) {
        this.config = _.merge(jenkinsConfig,_config)
        console.log(`_config:${_config?JSON.stringify(_config):'none'}`)
        console.log(`jenkinsConfig:${JSON.stringify(jenkinsConfig)}`)
        console.log(`this.config:${JSON.stringify(this.config)}`)
        console.log("welcome to my jenkins")
        console.log(`jenkinsConfig=${JSON.stringify(jenkinsConfig)}`)
        console.log(jenkinsConfig.getUrl())
    }
    stopJob(jobName,jobNumber) {
        return jenkins.build.stop(jobName,jobNumber)
    }
    jobsList() {
        return jenkins.job.list()
    }
    async jobNamesList() {
        return (await this.jobsList()).map(j=>j.name)
    }
    async serverInfo() {
        return (await jenkins.info())
    }
}

module.exports = MyJenkins