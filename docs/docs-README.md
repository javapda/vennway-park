# docs-README.md #

## Recent ##
* add test area (`npm run test`)

# install setup #
```
git clone git@bitbucket.org:javapda/vennway-park.git
cd vennway-park
npm install
```

# Resources #

* [lodash](https://lodash.com/) | [docs](https://lodash.com/docs/)
* [expressjs](https://expressjs.com/)
* [nodejs](https://nodejs.org/en/)
* [mongodb](https://www.mongodb.com/) | [docs](https://docs.mongodb.com/) | [ports](https://docs.mongodb.com/manual/reference/default-mongodb-port/) | [MongoDB driver for Node.js](https://www.npmjs.com/package/mongodb)

# Testing # 
* [jest](https://jestjs.io/) | [testing promises](https://jestjs.io/docs/en/asynchronous#promises)
* [jenkins support on macosx](https://java2blog.com/install-jenkins-mac-os-x/)

# Activity #

* added more jest testing