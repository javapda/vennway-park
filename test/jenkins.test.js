// https://www.npmjs.com/package/jenkins
var os = require("os");
console.log(`hostname: ${os.hostname()}`)
describe('jenkins', () => {
    test('minimum', () => {
        expect(true).toBeTruthy()
    })
})

if (os.hostname() === 'Johns-MacBook-Pro.local') {
    describe('testing jenkins', () => {
        const jenkinsCredentials = 'jenkins_jedi:hampster'
        const jenkinsHost = 'gs-ci.ad.goldblattsystems.com'
        const jenkinsUrl = `http://${jenkinsCredentials}@${jenkinsHost}`
        test('info', () => {
            var jenkins = require('jenkins')({ baseUrl: `${jenkinsUrl}`, crumbIssuer: true, promisify: true });
            expect(jenkins).toBeTruthy()
            expect(jenkinsUrl).toBe("http://jenkins_jedi:hampster@gs-ci.ad.goldblattsystems.com")
        })
    })
}