const _ = require('lodash')
describe('lodash tests', () => {
    test('uniq', () => {
        expect(_.uniq([1, 2, 3, 2])).toEqual([1, 2, 3])
        expect(_.sortedUniq([1, 2, 2, 3])).toEqual([1, 2, 3])
    })
    test('is', () => {
        expect(_.isNumber(3)).toBeTruthy()
        expect(_.isNumber('3')).not.toBeTruthy()
        expect(_.isString('jed')).toBeTruthy()
        expect(_.isString(5)).not.toBeTruthy()
    })
})
describe('time', () => {
    test('first test', () => {
        expect(true).toBeTruthy()
    })
})