
/**
 * 
 * https://www.npmjs.com/package/jenkins
 * https://jestjs.io/docs/en/asynchronous#promises
 * 
 */
var os = require("os");
console.log(`hostname: ${os.hostname()}`)
describe('gs-ci myjenkins', () => {
    test('minimum',()=>{
        expect(true).toBeTruthy()
    })
})
if (os.hostname() === 'Johns-MacBook-Pro.local') {
    describe('gs-ci myjenkins', () => {
        const MyJenkins = require('../src/myjenkins')
        const _ = require('lodash')
        const myjenkins = new MyJenkins({ credentials: 'jenkins_jedi:hampster', ssl: true })
        const expectedJobCount = 76

        test('jobsList', () => {
            return myjenkins.jobsList().then((data) => {
                expect(data).toBeTruthy()
                expect(_.isArray(data)).toBeTruthy()
                expect(data.length).toBe(expectedJobCount)
            });
        })
        test('jobNamesList', () => {
            return myjenkins.jobNamesList().then((data) => {
                expect(data).toBeTruthy()
                expect(_.isArray(data)).toBeTruthy()
                expect(data.length).toBe(expectedJobCount)
            });
        })
    })
}