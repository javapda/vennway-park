const net = require('net')
// https://nodejs.org/docs/latest-v9.x/api/net.html#net_socket_connect_port_host_connectlistener
const axios = require('axios')
const  isListenerUp = (host, port) => {
    console.log(`try ${host}:${port}`)
    const url = `http://${host}:${port}`
    
    return axios({
        method:'get',
        timeout:1000,
        url})
    .then((res)=>{})
    .catch((error)=>{
        // console.log(`OUOCUCOU, err=${JSON.stringify(err)}`)
        if (error.response) {
            // The request was made and the server responded with a status code
            // that falls out of the range of 2xx
            // console.log(error.response.data);
            console.log(error.response.status);
            console.log(error.response.headers);
          } else if (error.request) {
            // The request was made but no response was received
            // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
            // http.ClientRequest in node.js
            // console.log(`NO HOST: ${error.request}`);
            throw `Unable to connect to url ${url} - bad host [${host}]? bad port number [${port}]?`
          } else {
            // Something happened in setting up the request that triggered an Error
            console.log('Error', error.message);
          }
    })
}

isListenerUp('localhost',8080) || console.log(`bad localhost:8080`)