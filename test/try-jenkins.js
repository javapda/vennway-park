const jenkinsCredentials = 'jenkins_jedi:hampster'
const jenkinsHost = 'gs-ci.ad.goldblattsystems.com'
const jenkinsUrl = `http://${jenkinsCredentials}@${jenkinsHost}`
const jenkins = require('jenkins')({ baseUrl: `${jenkinsUrl}`, crumbIssuer: true, promisify: true });
jenkins.info()
.then((data)=>{
    // console.log('data',data)
})
.catch((err)=>{
    console.log('err',err)
})
//console.log(jenkins.info)


jenkins.build.get('cleanup-workspaces', 49)
.then((res)=>{
 console.log(res)
})
.catch((err)=>{
    console.log(`err=${err}`)
})

jenkins.job.list()
.then((res)=>{
    console.log("JOBS:"+res.map((j)=>j.name))
})