const program = require('commander')
const MyJenkins = require('./src/myjenkins')
const fs = require('fs');
const _ = require('lodash')
const version = '0.0.1'
var vennwayParkConfig = {jenkins:{}}
const homedir = require('os').homedir()
program
  .version(version)
  .option('--config-file <path_to_config>', 'file containing .json configuration information', `${homedir}/.vennway-park`)
  .option('--jenkins-host <host_name_or_ip>', 'host running jenkins', 'localhost')
  .option('--jenkins-port <port_number>', 'port number for jenkins', 8080)
  .option('--jenkins-use-ssl <true_or_false>', 'true=https, false=http', false)
  .option('--dump-config','output the final configuration to the console')


program.on('--help', function () {
  console.log('')
  console.log('Examples:');
  console.log(`  $ ${__filename} `);
  console.log(`  $ ${__filename} --help`);
})


program.parse(process.argv);

if (program.configFile) {
  if (!fs.existsSync(program.configFile)) {
    // throw `missing file ${program.configFile}`
    console.log(`missing file ${program.configFile}`)
  } else {
    try {
      fs.accessSync(program.configFile, fs.constants.R_OK)
    } catch (err) {
      throw `cannot read file ${program.configFile} - but it exists`
    }
  }
  var fileConfig = JSON.parse(fs.readFileSync(program.configFile, 'utf8'));
  vennwayParkConfig = _.merge(vennwayParkConfig, fileConfig)
}
if (program.jenkinsHost) {
  vennwayParkConfig.jenkins.host = program.jenkinsHost
}
if(program.jenkinsUseSsl) {
  vennwayParkConfig.jenkins.ssl = program.jenkinsUseSsl
}
if(program.jenkinsPort) {
  vennwayParkConfig.jenkins.port = parseInt(program.jenkinsPort)
}
if(program.dumpConfig) {
  console.log(`
START CONFIG:***********************
${JSON.stringify(vennwayParkConfig)}
END CONFIG:***********************    
    `)
}

const myjenkins = new MyJenkins(vennwayParkConfig)

console.log(`find server info`)
// myjenkins.serverInfo()
// .then((info)=>console.log('jenkins is up'))
// .catch((err)=>console.log(`problem: err=${err}`))
